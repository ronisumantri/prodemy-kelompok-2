package id.arnugroho.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import id.arnugroho.springboot.model.entity.Regency;
import java.util.List;

@Repository
public interface RegencyRepository extends JpaRepository<Regency, String> {
	
	List<Regency> findAllByProvinsiKodeProvinsi(String kodeProvinsi);
	
}
