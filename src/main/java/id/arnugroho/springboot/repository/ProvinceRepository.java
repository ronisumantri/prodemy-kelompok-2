package id.arnugroho.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import id.arnugroho.springboot.model.entity.Province;

@Repository
public interface ProvinceRepository extends JpaRepository<Province, String> {
}
