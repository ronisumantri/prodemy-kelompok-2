package id.arnugroho.springboot.repository;

import id.arnugroho.springboot.model.entity.Kecamatan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KecamatanRepository extends JpaRepository<Kecamatan, String> {
    List<Kecamatan> findAllByProvinceKodeProvinsi(String codeProvince);
}
