package id.arnugroho.springboot.repository;

import id.arnugroho.springboot.model.entity.Regency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import id.arnugroho.springboot.model.entity.Desa;

import java.util.List;


@Repository
    public interface DesaRepository extends JpaRepository<Desa,String>{
    List<Desa> findAllByProvinsiKodeProvinsi(String kodeProvinsi);
}

