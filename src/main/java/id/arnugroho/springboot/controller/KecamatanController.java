package id.arnugroho.springboot.controller;

import id.arnugroho.springboot.model.dto.KecamatanDto;
import id.arnugroho.springboot.model.entity.Regency;
import id.arnugroho.springboot.model.entity.Kecamatan;
import id.arnugroho.springboot.model.entity.Province;
import id.arnugroho.springboot.repository.RegencyRepository;
import id.arnugroho.springboot.repository.KecamatanRepository;
import id.arnugroho.springboot.repository.ProvinceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/prov/kab/kec")
public class KecamatanController {
    private final KecamatanRepository kecamatanRepository;
    private final RegencyRepository kabupatenRepository;
    private final ProvinceRepository provinceRepository;

    @Autowired
    public KecamatanController(KecamatanRepository kecamatanRepository, RegencyRepository kabupatenRepository, ProvinceRepository provinceRepository) {
        this.kecamatanRepository = kecamatanRepository;
        this.kabupatenRepository = kabupatenRepository;
        this.provinceRepository = provinceRepository;
    }


    // http://localhost:8080/kecamatan
    @GetMapping
    public List<KecamatanDto> get() {
        List<Kecamatan> kecamatanList = kecamatanRepository.findAll();
        List<KecamatanDto> kecamatanDtoList = kecamatanList.stream().map(this::convertToDto)
                .collect(Collectors.toList());
        return kecamatanDtoList;
    }

    // http://localhost:8080/kecamatan/3303
    @GetMapping("/{code}")
    public KecamatanDto get(@PathVariable String code) {
        if(kecamatanRepository.findById(code).isPresent()){
            KecamatanDto kecamatanDto =  convertToDto(kecamatanRepository.findById(code).get());
            return kecamatanDto;
        }
        return null;
    }

    @GetMapping("/prov/{codeProvince}")
    public List<KecamatanDto> getByProvince(@PathVariable String codeProvince) {
        List<Kecamatan> kecamatanList = kecamatanRepository.findAllByProvinceKodeProvinsi(codeProvince);
        List<KecamatanDto> kecamatanDtoList = kecamatanList.stream().map(this::convertToDto)
                .collect(Collectors.toList());
        return kecamatanDtoList;
    }
    /*Insert Data*/
    @PostMapping
    public KecamatanDto insert(@RequestBody KecamatanDto dto) {
        Kecamatan kecamatan = convertToEntity(dto);
        kecamatanRepository.save(kecamatan);
        return convertToDto(kecamatan);
    }

    private Kecamatan convertToEntity(KecamatanDto dto){
        Kecamatan kecamatan = new Kecamatan();
        kecamatan.setKodeKecamatan(dto.getCodeSubDistrict());
        kecamatan.setNamaKecamatan(dto.getNameSubDistrict());

        if(provinceRepository.findById(dto.getCodeProvince()).isPresent()){
            Province province =  provinceRepository.findById(dto.getCodeProvince()).get();
            kecamatan.setProvince(province);
        }
        if(kabupatenRepository.findById(dto.getCodeRegency()).isPresent()){
            Regency kabupaten =  kabupatenRepository.findById(dto.getCodeRegency()).get();
            kecamatan.setKabupaten(kabupaten);
        }

        return kecamatan;
    }

    private KecamatanDto convertToDto(Kecamatan kecamatan){
        KecamatanDto dto = new KecamatanDto();
        dto.setCodeSubDistrict(kecamatan.getKodeKecamatan());
        dto.setNameSubDistrict(kecamatan.getNamaKecamatan());
        dto.setCodeProvince(kecamatan.getProvince().getKodeProvinsi());
        dto.setNameProvince(kecamatan.getProvince().getNamaProvinsi());
        dto.setCodeRegency(kecamatan.getKabupaten().getKodeKabupaten());
        dto.setNameRegency(kecamatan.getKabupaten().getNamaKabupaten());
        return dto;
    }
}
