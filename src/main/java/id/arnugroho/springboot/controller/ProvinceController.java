package id.arnugroho.springboot.controller;

import id.arnugroho.springboot.model.dto.ProvinceDto;
import id.arnugroho.springboot.model.entity.Province;
import id.arnugroho.springboot.repository.ProvinceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/prov")
public class ProvinceController {

	private final ProvinceRepository provinceRepository;

	@Autowired
	public ProvinceController(ProvinceRepository provinceRepository) {
		this.provinceRepository = provinceRepository;
	}

	// http://localhost:8080/prov/
	@GetMapping("/")
	public List<ProvinceDto> get() {
		List<Province> provinceList = provinceRepository.findAll();
		List<ProvinceDto> provinceDtoList = provinceList.stream().map(this::convertToDto).collect(Collectors.toList());
		return provinceDtoList;
	}

	// http://localhost:8080/prov/33
	@GetMapping("/{code}")
	public ProvinceDto get(@PathVariable String code) {
		if (provinceRepository.findById(code).isPresent()) {
			ProvinceDto provinceDto = convertToDto(provinceRepository.findById(code).get());
			return provinceDto;
		}
		return null;
	}

	/* Insert Data */
	@PostMapping
	public ProvinceDto insert(@RequestBody ProvinceDto provinceDto) {
		Province province = convertToEntity(provinceDto);
		provinceRepository.save(province);
		return convertToDto(province);
	}

	private Province convertToEntity(ProvinceDto provinceDto) {
		Province province = new Province();
		province.setKodeProvinsi(provinceDto.getCodeProvince());
		province.setNamaProvinsi(provinceDto.getNameProvince());
		return province;
	}

	private ProvinceDto convertToDto(Province province) {
		ProvinceDto provinceDto = new ProvinceDto();
		provinceDto.setCodeProvince(province.getKodeProvinsi());
		provinceDto.setNameProvince(province.getNamaProvinsi());
		return provinceDto;
	}

}
