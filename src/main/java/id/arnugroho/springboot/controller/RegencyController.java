package id.arnugroho.springboot.controller;


import id.arnugroho.springboot.model.dto.RegencyDto;
import id.arnugroho.springboot.model.entity.Province;
import id.arnugroho.springboot.model.entity.Regency;
import id.arnugroho.springboot.repository.ProvinceRepository;
import id.arnugroho.springboot.repository.RegencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/prov/kab")

public class RegencyController {

	private final RegencyRepository regencyRepository;
	private final ProvinceRepository provinceRepository;

	@Autowired
	public RegencyController(RegencyRepository regencyRepository, ProvinceRepository provinceRepository) {
		this.regencyRepository = regencyRepository;
		this.provinceRepository = provinceRepository;
	}

	// http://localhost:8080/prov/kab/
	@GetMapping("/")
	public List<RegencyDto> getRegency() {
		List<Regency> regencyList = regencyRepository.findAll();
		List<RegencyDto> regencyDtoList = regencyList.stream().map(this::convertToDto).collect(Collectors.toList());
		return regencyDtoList;
	}

	// http://localhost:8080/prov/kab/3204
	@GetMapping("/{code}")
	public RegencyDto getRegency(@PathVariable String code) {
		if (regencyRepository.findById(code).isPresent()) {
			RegencyDto regencyDto = convertToDto(regencyRepository.findById(code).get());
			return regencyDto;
		}
		return null;
	}

	@GetMapping("/prov/{codeProvince}")
	public List<RegencyDto> getByProvince(@PathVariable String codeProvince) {
		List<Regency> regencyList = regencyRepository.findAllByProvinsiKodeProvinsi(codeProvince);
		List<RegencyDto> regencyDtoList = regencyList.stream().map(this::convertToDto).collect(Collectors.toList());
		return regencyDtoList;
	}

	/* Insert Data */
	@PostMapping
	public RegencyDto insert(@RequestBody RegencyDto regencyDto) {
		Regency regency = convertToEntity(regencyDto);
		regencyRepository.save(regency);
		return convertToDto(regency);
	}

	private Regency convertToEntity(RegencyDto regencyDto) {
		Regency regency = new Regency();
		regency.setKodeKabupaten(regencyDto.getCodeRegency());
		regency.setNamaKabupaten(regencyDto.getNameRegency());

		if (provinceRepository.findById(regencyDto.getCodeProvince()).isPresent()) {
			Province province = provinceRepository.findById(regencyDto.getCodeProvince()).get();
			regency.setProvinsi(province);
		}

		return regency;
	}

	private RegencyDto convertToDto(Regency regency) {
		RegencyDto regencyDto = new RegencyDto();
		regencyDto.setCodeRegency(regency.getKodeKabupaten());
		regencyDto.setNameRegency(regency.getNamaKabupaten());
		regencyDto.setCodeProvince(regency.getProvinsi().getKodeProvinsi());
		regencyDto.setNameProvince(regency.getProvinsi().getNamaProvinsi());
		return regencyDto;
	}
}
