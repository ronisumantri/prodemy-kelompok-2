package id.arnugroho.springboot.controller;

import id.arnugroho.springboot.model.entity.Province;
import id.arnugroho.springboot.model.entity.Regency;
import id.arnugroho.springboot.model.entity.Kecamatan;
import id.arnugroho.springboot.model.entity.Desa;
import id.arnugroho.springboot.model.dto.RegencyDto;
import id.arnugroho.springboot.model.dto.KecamatanDto;
import id.arnugroho.springboot.model.dto.DesaDto;
import id.arnugroho.springboot.repository.ProvinceRepository;
import id.arnugroho.springboot.repository.RegencyRepository;
import id.arnugroho.springboot.repository.KecamatanRepository;
import id.arnugroho.springboot.repository.DesaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/prov/kab/kec/des")
public class DesaController {
    // http://localhost:8080/prov/kab/kec/des/
    private final DesaRepository desaRepository;
    private final KecamatanRepository kecamatanRepository;
    private final RegencyRepository regencyRepository;
    private final ProvinceRepository provinceRepository;

    @Autowired
    public DesaController(DesaRepository desaRepository, KecamatanRepository kecamatanRepository, RegencyRepository regencyRepository,ProvinceRepository provinceRepository) {
        this.desaRepository = desaRepository;
        this.kecamatanRepository = kecamatanRepository;
        this.regencyRepository = regencyRepository;
        this.provinceRepository = provinceRepository;
    }

    @GetMapping("/")
    public List<DesaDto> get() {
        List<Desa> desaList = desaRepository.findAll();
        List<DesaDto> desaDtoList = desaList.stream().map(this::convertToDto)
                .collect(Collectors.toList());
        return desaDtoList;
    }

    // http://localhost:8080/prov/kab/kec/des/33
    @GetMapping("/{code}")
    public DesaDto get(@PathVariable String code) {
        if (desaRepository.findById(code).isPresent()) {
            DesaDto desaDto = convertToDto(desaRepository.findById(code).get());
            return desaDto;
        }
        return null;
    }

    @GetMapping("/prov/{codeProvince}")
    public List<DesaDto> getByProvince(@PathVariable String codeProvince) {
        List<Desa> desaList = desaRepository.findAllByProvinsiKodeProvinsi(codeProvince);
        List<DesaDto> desaDtoList = desaList.stream().map(this::convertToDto).collect(Collectors.toList());
        return desaDtoList;
    }

    /*Insert Data*/
    @PostMapping
    public DesaDto insert(@RequestBody DesaDto desaDto) {
        Desa desa = convertToEntity(desaDto);
        desaRepository.save(desa);
        return convertToDto(desa);
    }

    private Desa convertToEntity(DesaDto desaDto){
        Desa desa = new Desa();
        desa.setCodeDesa(desaDto.getCodeDesa());
        desa.setNameDesa(desaDto.getNameDesa());

        if (provinceRepository.findById(desaDto.getCodeProvince()).isPresent()){
            Province province = provinceRepository.findById(desaDto.getCodeProvince()).get();
            desa.setProvinsi(province);
        }
        if (regencyRepository.findById(desaDto.getCodeRegency()).isPresent()){
            Regency regency = regencyRepository.findById(desaDto.getCodeSubDistrict()).get();
            desa.setRegency(regency);
        }
        if (kecamatanRepository.findById(desaDto.getCodeSubDistrict()).isPresent()){
            Kecamatan kecamatan = kecamatanRepository.findById(desaDto.getCodeSubDistrict()).get();
            desa.setKecamatan(kecamatan);
        }
        return desa;
    }

    private DesaDto convertToDto(Desa desa){
        DesaDto desaDto = new DesaDto();
        desaDto.setCodeDesa(desa.getCodeDesa());
        desaDto.setNameDesa(desa.getNameDesa());
        desaDto.setCodeSubDistrict(desa.getKecamatan().getKodeKecamatan());
        desaDto.setNameSubDistrict(desa.getKecamatan().getNamaKecamatan());
        desaDto.setCodeRegency(desa.getRegency().getKodeKabupaten());
        desaDto.setNameRegency(desa.getRegency().getNamaKabupaten());
        desaDto.setCodeProvince(desa.getProvinsi().getKodeProvinsi());
        desaDto.setNameProvince(desa.getProvinsi().getNamaProvinsi());

        return desaDto;
    }
}

