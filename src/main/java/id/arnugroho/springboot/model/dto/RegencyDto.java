package id.arnugroho.springboot.model.dto;

public class RegencyDto {
	private String codeRegency;
	private String nameRegency;
	private String codeProvince;
	private String nameProvince;

	public String getCodeRegency() {
		return codeRegency;
	}

	public void setCodeRegency(String codeRegency) {
		this.codeRegency = codeRegency;
	}

	public String getNameRegency() {
		return nameRegency;
	}

	public void setNameRegency(String nameRegency) {
		this.nameRegency = nameRegency;
	}

	public String getCodeProvince() {
		return codeProvince;
	}

	public void setCodeProvince(String codeProvince) {
		this.codeProvince = codeProvince;
	}

	public String getNameProvince() {
		return nameProvince;
	}

	public void setNameProvince(String nameProvince) {
		this.nameProvince = nameProvince;
	}

}
