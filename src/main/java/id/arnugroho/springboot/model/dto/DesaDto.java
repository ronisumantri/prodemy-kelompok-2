package id.arnugroho.springboot.model.dto;

public class DesaDto {
    private String codeDesa;
    private String nameDesa;
    private String codeSubDistrict;
    private String nameSubDistrict;
    private String codeRegency;
    private String nameRegency;
    private String codeProvince;
    private String nameProvince;


    public String getCodeDesa() {
        return codeDesa;
    }

    public void setCodeDesa(String codeDesa) {
        this.codeDesa = codeDesa;
    }

    public String getNameDesa() {
        return nameDesa;
    }

    public void setNameDesa(String nameDesa) {
        this.nameDesa = nameDesa;
    }

    public String getCodeSubDistrict(){
        return codeSubDistrict;
    }

    public void setCodeSubDistrict(String codeSubDistrict){
        this.codeSubDistrict = codeSubDistrict;
    }

    public String getNameSubDistrict(){
        return nameSubDistrict;
    }

    public void setNameSubDistrict(String subDistrict){
        this.nameSubDistrict = subDistrict;
    }

    public String getCodeRegency() {
        return codeRegency;
    }

    public void setCodeRegency(String codeRegency) {
        this.codeRegency = codeRegency;
    }

    public String getNameRegency() {
        return nameRegency;
    }

    public void setNameRegency(String nameRegency) {
        this.nameRegency = nameRegency;
    }

    public String getCodeProvince() {
        return codeProvince;
    }

    public void setCodeProvince(String codeProvince) {
        this.codeProvince = codeProvince;
    }

    public String getNameProvince() {
        return nameProvince;
    }

    public void setNameProvince(String nameProvince) {
        this.nameProvince = nameProvince;
    }

}
