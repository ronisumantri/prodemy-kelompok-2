package id.arnugroho.springboot.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_des")
public class Desa {
    @Id
    @Column(name="kddes",length=25)
    private String codeDesa;

    @Column(name="nmdes")
    private String nameDesa;

    @ManyToOne
    @JoinColumn(name = "kdkec",nullable = false)
    private Kecamatan kecamatan;

    @ManyToOne
    @JoinColumn(name = "cdreg",nullable = false)
    private Regency regency;

    @ManyToOne
    @JoinColumn(name = "cdprov",nullable = false)
    private Province provinsi;

    public String getCodeDesa() {
        return codeDesa;
    }

    public void setCodeDesa(String codeDesa) {
        this.codeDesa = codeDesa;
    }

    public String getNameDesa() {
        return nameDesa;
    }

    public void setNameDesa(String nameDesa) {
        this.nameDesa = nameDesa;
    }

    public Kecamatan getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(Kecamatan kecamatan) {
        this.kecamatan = kecamatan;
    }

    public Regency getRegency() {
        return regency;
    }

    public void setRegency(Regency regency) {
        this.regency = regency;
    }

    public Province getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(Province provinsi) {
        this.provinsi = provinsi;
    }
}
