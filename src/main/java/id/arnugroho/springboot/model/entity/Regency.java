package id.arnugroho.springboot.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_regency")
public class Regency {
	@Id
	@Column(name = "cdreg", length = 25)
	private String kodeKabupaten;

	@Column(name = "nmreg")
	private String namaKabupaten;

	@ManyToOne
	@JoinColumn(name = "cdprov", nullable = false)
	private Province provinsi;

	public String getKodeKabupaten() {
		return kodeKabupaten;
	}

	public void setKodeKabupaten(String kodeKabupaten) {
		this.kodeKabupaten = kodeKabupaten;
	}

	public String getNamaKabupaten() {
		return namaKabupaten;
	}

	public void setNamaKabupaten(String namaKabupaten) {
		this.namaKabupaten = namaKabupaten;
	}

	public Province getProvinsi() {
		return provinsi;
	}

	public void setProvinsi(Province provinsi) {
		this.provinsi = provinsi;
	}

}
