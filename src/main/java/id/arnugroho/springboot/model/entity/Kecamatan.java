package id.arnugroho.springboot.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_kecamatan")
public class Kecamatan {
    @Id
    @Column(name = "cdkec", length = 25)
    private String kodeKecamatan;

    @Column(name = "nmkec")
    private String namaKecamatan;

    @ManyToOne
    @JoinColumn(name="cdreg", nullable = false)
    private Regency kabupaten;

    @ManyToOne
    @JoinColumn(name="cdprov", nullable = false)
    private Province province;

	public String getKodeKecamatan() {
		return kodeKecamatan;
	}

	public void setKodeKecamatan(String kodeKecamatan) {
		this.kodeKecamatan = kodeKecamatan;
	}

	public String getNamaKecamatan() {
		return namaKecamatan;
	}

	public void setNamaKecamatan(String namaKecamatan) {
		this.namaKecamatan = namaKecamatan;
	}

	public Regency getKabupaten() {
		return kabupaten;
	}

	public void setKabupaten(Regency kabupaten) {
		this.kabupaten = kabupaten;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

   
}
